from Adivinhação import main as adivinhacao
from Forca import main as forca
from Mega_Sena import main as mega_sena
from Velha import main as jogo_velha


def start():
	print("*******************")
	print("Escolha o seu jogo!")
	print("*******************")

	print("(1) Forca (2) Adivinhação (3) Mega Sena (4) Jogo da Velha")

	game = int(input("Qual jogo deseja iniciar?\n"))

	if game == 1:
		print("Jogando Forca")
		forca.play()
	elif game == 2:
		print("Jogando Adivinhação")
		adivinhacao.play()
	elif game == 3:
		print("Jogando Mega Sena")
		mega_sena.play()
	elif game == 4:
		print("Jogando Jogo da Velha")
		jogo_velha.play()


if __name__ == '__main__':
	start()
