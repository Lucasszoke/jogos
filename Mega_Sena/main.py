import random


def sort_numbers():
	sorted_numbers = []
	index = 0

	while index < 6:
		sorted_numbers.append(random.randrange(1, 61))
		index += 1

	return sorted_numbers


def verify_attemps(numbers_played, sorted_numbers):
	hits = 0

	for number_played in numbers_played:
		if number_played in sorted_numbers:
			hits += 1

	return hits


def welcome():
	print("Bem vindo ao jogo de Mega_Sena!")


def play():
	welcome()
	sorted_numbers = sort_numbers()
	attemps = number_of_attemps()
	numbers_played = []

	while len(numbers_played) < attemps:
		number_played = get_played_number(numbers_played)
		numbers_played.append(number_played)

	hits = verify_attemps(numbers_played, sorted_numbers)

	print("Acertou um total de ", hits, " números")

	print("Os números sorteados foram: ", sorted_numbers)


def get_played_number(numbers_played):
	played = int(input("Digite o número que deseja jogar: "))
	if played > 0 and played <= 60:
		if played in numbers_played:
			print("Número já foi jogado, por favor selecione outro número")
			get_played_number(numbers_played)
		else:
			return played
	else:
		print("O número jogado não atende o padrão de 1 a 60, por favor selecione outro número")
		get_played_number(numbers_played)


def number_of_attemps():
	try:
		attemps = int(input("Quantos números deseja jogar? Escolha entre 6 e 15 números: "))
		if attemps >= 6 and attemps <= 15:
			show_info_attemps(attemps)
			return int(attemps)
		else:
			print("Número escrito não atende os padrões de 6 a 15 números, irei repetir a pergunta")
			number_of_attemps()
	except:
		print("São aceitos somente números nessa seleção, irei repetir a pergunta")
		number_of_attemps()


def show_info_attemps(attemps):
	print({
		6: 'Valor da aposta: 3,50 | Sua chance de ganhar é 1 em 50.063.860',
		7: 'Valor da aposta: 24,50 | Sua chance de ganhar é 1 em 7.151.980',
		8: 'Valor da aposta: 98,00 | Sua chance de ganhar é 1 em 1.787.995',
		9: 'Valor da aposta: 294,00 | Sua chance de ganhar é 1 em 595.998',
		10: 'Valor da aposta: 735,00 | Sua chance de ganhar é 1 em 238.399',
		11: 'Valor da aposta: 1.617,00 | Sua chance de ganhar é 1 em 108.363',
		12: 'Valor da aposta: 3.234,00 | Sua chance de ganhar é 1 em 54.182',
		13: 'Valor da aposta: 6.006,00 | Sua chance de ganhar é 1 em 29.175',
		14: 'Valor da aposta: 10.510,50 | Sua chance de ganhar é 1 em 16.671',
		15: 'Valor da aposta: 17.517,50 | Sua chance de ganhar é 1 em 10.003',
	}[attemps])


if __name__ == '__main__':
	play()
