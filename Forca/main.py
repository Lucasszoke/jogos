import random


def welcome():
	print("Bem vindo ao jogo da forca!")


def load_secret_word():
	if __name__ == '__main__':
		archive = open("secret_words.txt", "r")
	else:
		archive = open("./Forca/secret_words.txt", "r")
	words = []

	for line in archive:
		line = line.strip()
		words.append(line)

	archive.close()
	random_number = random.randrange(0, len(words))
	secret_word = words[random_number].upper()
	return secret_word


def play():

	welcome()
	secret_word = load_secret_word()

	lose = False
	win = False
	char_list_played_hit = ["_" for char in secret_word]
	char_list_played = []
	errors = 0

	print(char_list_played_hit)

	while not lose and not win:
		selected_char = input("Digite uma letra para tentar adivinhar a palavra: ")
		formated_char = selected_char.strip().upper()

		if formated_char in char_list_played:
			print("Essa letra já foi jogada")
		else:
			char_list_played.append(formated_char)
			if formated_char in secret_word:
				index = 0
				for char in secret_word:
					if formated_char == char:
						print("Encontrei a letra {} na posição {}".format(selected_char, index+1))
						char_list_played_hit[index] = char
						# print(char_list_played)
					index += 1
			else:
				print("Não encontrei a letra {} na palavra sorteada".format(selected_char))
				errors += 1
			lose = errors == 6
			win = "_" not in char_list_played_hit

		print(char_list_played_hit)

	if win:
		print("Parabéns, você ganhou o jogo!")
	else:
		print("Você perdeu, que pena...")


if __name__ == '__main__':
	play()