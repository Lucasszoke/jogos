values_played = []
player1 = 'Jogador 1'
player2 = 'Jogador 2'
is_finished = False


class ScreenGame:
	def __init__(self, line_a, line_b, line_c):
		self.line_a = line_a
		self.line_b = line_b
		self.line_c = line_c


def play():
	global player1
	global player2
	global is_finished
	player1 = input('Digite o nome do jogador 1: ')
	player2 = input('Digite o nome do jogador 2: ')
	screen = ScreenGame(['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_'])
	show_screen(screen)
	for index in range(9):
		# print(index)
		is_finished = check_if_finish(screen)
		if is_finished:
			print('O jogo terminou!')
			break
		screen = get_play(screen, define_player(index))
		show_screen(screen)
	if not is_finished:
		print('Empate, ninguém ganhou nessa rodada!')


def show_screen(screen):
	for line in vars(screen):
		# print(line)
		show_line = ''
		index = 0
		for value_line in screen.__getattribute__(line):
			if index == 2:
				show_line += value_line
			else:
				show_line += value_line + '\t'
			index += 1
		print(show_line)


def get_play(screen, player):
	try:
		line = int(input('Digite a linha que deseja jogar, escolha entre: 1, 2, ou 3: '))
		column = int(input('Digite a coluna que deseja jogar, escolha entre: 1, 2, ou 3: '))
		# print('Foi escolhida a posição {}:{}'.format(line, column))
		if line < 1 or line > 3 or column < 1 or column > 3:
			print('Só existem 3 colunas e linhas, por favor selecione um valor entre esses!')
			get_play(screen, player)
		if '{}:{}'.format(line, column) in values_played:
			print('A linha e coluna já foram selecionadas! Por favor escolha novamente')
			get_play(screen, player)
		else:
			values_played.append('{}:{}'.format(line, column))
			index_line = 0
			for line_screen in vars(screen):
				index_line += 1
				if index_line == line:
					# print('Essa linha foi escolhida: {}'.format(line_screen))
					index_column = 0
					for column_screen in screen.__getattribute__(line_screen):
						index_column += 1
						if index_column == column:
							# print('Essa coluna foi escolhida: {}'.format(column_screen))
							screen.__getattribute__(line_screen)[index_column-1] = player
		return screen
	except:
		print('Digite um valor válido, irei repetir a pergunta')
		get_play(screen, player)
		return screen


def check_if_finish(screen):
	if screen.line_a[0] == screen.line_a[1] == screen.line_a[2] and screen.line_a[0] != '_':
		print('Vitória através da primeira linha pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_b[0] == screen.line_b[1] == screen.line_b[2] and screen.line_b[0] != '_':
		print('Vitória através da segunda linha pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_c[0] == screen.line_c[1] == screen.line_c[2] and screen.line_c[0] != '_':
		print('Vitória através da terceira linha pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_a[0] == screen.line_b[0] == screen.line_c[0] and screen.line_a[0] != '_':
		print('Vitória através da primeira coluna pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_a[1] == screen.line_b[1] == screen.line_c[1] and screen.line_a[1] != '_':
		print('Vitória através da segunda coluna pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_a[2] == screen.line_b[2] == screen.line_c[2] and screen.line_a[2] != '_':
		print('Vitória através da terceira coluna pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_a[0] == screen.line_b[1] == screen.line_c[2] and screen.line_a[0] != '_':
		print('Vitória através da diagonal de cima para baixo pelo {}'.format(get_player(screen.line_a[0])))
		return True
	elif screen.line_a[2] == screen.line_b[1] == screen.line_c[0] and screen.line_c[0] != '_':
		print('Vitória através da diagonal de baixo para cima pelo {}'.format(get_player(screen.line_a[0])))
		return True
	else:
		return False


def define_player(index):
	if index % 2 == 0:
		return 'X'
	else:
		return 'O'


def get_player(value_column):
	if value_column == 'X':
		return player1
	else:
		return player2


if __name__ == '__main__':
	play()
