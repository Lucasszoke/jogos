import random


def play():
	print("Bem vindo ao jogo de adivinhação!")

	secret_number = round(random.randrange(1, 101))
	attemps = 1
	score = 1000

	print("Qual nível de dificuldade  deseja jogar?")
	print("(1) Fácil (2) Médio (3) Difícil")

	level = int(input("Defina o nível: "))

	if level == 1:
		maximum_attemps = 20
	elif level == 2:
		maximum_attemps = 10
	elif level == 3:
		maximum_attemps = 5
	else:
		print("Valor digitado inválido, será definido dificuldade Fácil")
		maximum_attemps = 20

	while attemps <= maximum_attemps:
		print("Tentativa {} de {}".format(attemps, maximum_attemps))
		selected_number = int(input("Digite um número entre 1 e 100: "))

		print("Você digitou ", selected_number)

		hit = selected_number == secret_number
		bigger = selected_number > secret_number
		minor = selected_number < secret_number
		if selected_number < 1 or selected_number > 100:
			print("Você deve digitar um número entre 1 e 100!")
			continue

		if hit:
			print("Você acertou e fez {} pontos".format(score))
			break
		else:
			if bigger:
				print("Você errou, seu chute foi maior do que o número sorteado")
			elif minor:
				print("Você errou, seu chute foi menor do que o número sorteado")
			scoreLoss = abs(secret_number - selected_number)
			score -= scoreLoss
		print("***************************")
		attemps += 1


if __name__ == '__main__':
	play()